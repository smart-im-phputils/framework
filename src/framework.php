<?php

if (!function_exists('handle_cors_request')) {
    /**
     * Handles a CORS request
     */
    function handle_cors_request($extra = []) {
        if ($origin = $_SERVER['HTTP_ORIGIN'] ?? NULL) {
            if (function_exists('cors_allowed') && !cors_allowed($origin)) {
                return;
            }

            $headers = array_merge([
                'Access-Control-Allow-Origin'      => $origin ?? '*',
                'Access-Control-Allow-Methods'     => env('CORS_METHODS', 'POST, GET, OPTIONS, PUT, DELETE'),
                'Access-Control-Allow-Credentials' => 'true',
                'Access-Control-Allow-Headers'     => 'x-cache-buster, x-xsrf-token, origin, content-type, accept',
                'Access-Control-Max-Age'           => is_debug() ? 0 : 86400,
            ], $extra);

            foreach ($headers as $key => $value) {
                header("$key: $value", TRUE);
            }
        }
    }
}

if (!function_exists('get_domain')) {
    /**
     * Returns domain of the app
     */
    function get_domain() {
        return @env('APP_DOMAIN', domain($_SERVER['HTTP_HOST'], TRUE));
    }
}

if (!function_exists('get_http_method')) {
    /**
     * Returns the HTTP method
     */
    function get_http_method($allowed = ['get', 'post', 'options']) {
        return @preg_match('/(' . join('|', $allowed) . ')/i', $_SERVER['REQUEST_METHOD'], $match) ? strtoupper($match[1]) : 'GET';
    }
}

if (!function_exists('check_http_method')) {
    /**
     * Returns the HTTP method
     */
    function check_http_method(...$allowed) {
        if (in_array_nc(get_http_method(), $allowed)) {
            return TRUE;
        }

        abort('Not found', 404);
    }
}

if (!function_exists('handle_upload')) {
    /**
     * Handles a file upload
     */
    function handle_upload($names = NULL, $destDir = NULL, $allowedExt = NULL) {
        foreach ($_FILES as $key => $value) {
            if (empty($names) || in_array($key, (array) $names)) {
                if (empty($allowedExt) || in_array(file_ext($value['name']), array_map_args('ltrim', (array) $allowedExt, '.'))) {
                    $fn = sprintf('%s/%s', realpath($destDir) ?: sys_get_temp_dir(), $value['name']);

                    if (move_uploaded_file($value['tmp_name'], $fn)) {
                        $results[] = $fn;
                    }
                }
            }
        }

        return $results;
    }
}

if (!function_exists('handle_json_request')) {
    /**
     * Process PHP json request
     */
    function handle_json_request() {
        if ($data = file_get_contents('php://input')) {
            $json = json_decode_array($data) ?: [];
            $_REQUEST = array_merge($_REQUEST, $json);

            if (get_http_method() === 'GET') {
                $_GET = array_merge($_GET, $json);
            } elseif (get_http_method() === 'POST') {
                $_POST = array_merge($_POST, $json);
            }
        }
    }
}

if (!function_exists('config')) {
    /**
     * Gets value from config file
     */
    function config($name, $default) {
        global $cache;

        if (!$conf = $cache['config'] ?? NULL) {
            if ($fn = app_dir('config.php')) {
                $conf = $cache['config'] = include_once($fn);
            }
        }

        return $conf[$name] ?? $default;
    }
}

if (!function_exists('url')) {
    /**
     * Creates a URL for current app
     */
    function url($url, $params = []) {
        $url = is_url($url) ? $url : absolute_uri($url, env('APP_URL', env('VUE_APP_URL')));

        return !empty($params) ? sprintf("%s%s%s", $url, strpos($url, '?') === FALSE ? '?' : '&', http_build_query($params)) : $url;
    }
}

if (!function_exists('auth_url')) {
    /**
     * Creates a pre-authenticated url
     */
    function auth_url($url, $user_id, $activity = 'link_click', $data = []) {
        $payload = array_merge($data, ['user_id' => $user_id, 'activity' => $activity, 'exp' => time() + 86400]);
        return url('/api/auth', ['jwt' => jwt_encode($payload), 'fwd' => url($url)]);
    }
}

if (!function_exists('is_ajax_request')) {
    /**
     * Check if it is a ajax request
     */
    function is_ajax_request() {
        return strpos($_SERVER['HTTP_ACCEPT'] ?? '', 'application/json') !== FALSE;
    }
}

if (!function_exists('login_redir')) {
    /**
     * Redirect to login page with error
     */
    function login_redir($message = '') {
        redirect(env('LOGIN_URL', '/login'), 302, $message);
    }
}

if (!function_exists('abort')) {
    /**
     * Aborts a request with 403 error code
     */
    function abort($message = '', $code = 403) {
        header("HTTP/1.0 $code Forbidden", TRUE, $code);
        echo is_ajax_request() ? json_encode(['data' => $message]) : ($message ?? '');
        return exit(0) ? TRUE : FALSE; //to prevent inconsistent return warnings
    }
}

if (!function_exists('redirect')) {
    /**
     * Redirect to another url
     */
    function redirect($location, $code = 302, $message = NULL) {
        if (is_ajax_request()) {
            abort($message);
        } else {
            header("Location: $location", TRUE, $code);
        }

        if (!empty($message)) {
            echo is_scalar($message) ? $message : json_encode($message);
        }

        exit;
    }
}

if (!function_exists('request')) {
    /**
     * returns a request param
     *
     * @return string|array
     */
    function request($name = NULL, $default = NULL) {
        $vars = array_merge($_GET, $_POST);

        return !empty($name) ? ($vars[$name] ?? $default) : $vars;
    }
}

if (!function_exists('request_keys')) {
    /**
     * Extracts all the keys from request (returns false if any key is missing)
     */
    function request_keys(...$keys) {
        if ($all = call_user_func_array('has_keys', array_merge([request()], $keys))) {
            return array_values($all);
        }

        return FALSE;
    }
}

if (!function_exists('request_keys_ignore_empty')) {
    /**
     * Extracts all the keys from request and only returns keys which have any data
     */
    function request_keys_ignore_empty(...$keys) {
        return call_user_func_array('pluck_keys_ignore_empty', array_merge([request()], $keys));
    }
}

if (!function_exists('add_route')) {
    /**
     * Adds a route for router
     */
    function add_route(string $path, $action, $method = '', $auth = FALSE) {
        return $GLOBALS['routes'][] = ['path' => $path, 'action' => $action, 'method' => strtoupper($method), 'auth' => $auth];
    }
}

if (!function_exists('router')) {
    /**
     * Simplest PHP router
     */
    function router() {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $path = $url['path'];

        foreach ($GLOBALS['routes'] ?? [] as $route) {
            $regex = (strpos($route['path'], '(') !== FALSE) && (strpos($route['path'], ')') !== FALSE);
            $urlMatch = $regex ? preg_match("#{$route['path']}#", $url['path'], $parts) : (trim($route['path'], '/') == trim($url['path'], '/'));

            if ($urlMatch && (empty($route['method']) || ($route['method'] === get_http_method()))) {
                $match = $route;
                break;
            }
        }

        if (!empty($match)) {
            if (empty($match['auth']) || !empty(user_id())) {
                $fun = $match['action'];

                if (is_scalar($fun)) {
                    if (($file = app_dir("$fun.php")) || ($file = app_dir("$fun.html")) || ($file = app_dir($fun))) {
                        $found = $result = include_once $file;
                    }
                } else {
                    $found = $result = call_user_func_array($fun, !empty($parts) ? array_slice($parts, 1) : []);
                }
            } else {
                login_redir('Login required');
            }
        } else {
            if (!($file = app_dir("$path.php") ?: app_dir("$path.html") ?: app_dir($path))) {
                do {
                    $path = dirname($path);
                    $file = app_dir("$path.php") ?: app_dir("$path.html") ?: app_dir($path); //for globs like site/* => site/create, site/remove, etc
                    $part = ltrim(substr($url['path'], strlen($path)), '/');
                } while ((strlen($path) > 1) && !file_exists($file));
            }

            if (is_file($file) && !is_dir($file)) {
                if ($found = (strpos($file, app_dir(dirname($_SERVER['PHP_SELF']))) === 0)) { //only include file under current directory
                    try {
                        ob_start();
                        $return = include_once $file;
                        $result = trim(ob_get_contents());
                        $result = $result !== '' ? $result : $return;
                    } catch (\Throwable $e) {
                        if (is_debug()) {
                            throw $e;
                        } else {
                            log_error($e->getMessage());
                        }
                    } finally {
                        ob_end_clean();
                    }
                };
            }
        }

        header('x-cache-buster: ' . (rand(99, 9999999) . '/' . time()));

        if (empty($found)) {
            header('404 Not found', TRUE, 404);
            echo '<h1>404 Not found</h1>';
        } elseif (is_array($result)) {
            header('Content-type: application/json', TRUE);

            echo json_encode($result);
        } elseif (is_scalar($result)) {
            echo $result;
        }
    }
}

if (!function_exists('jwt_cookie_name')) {
    /**
     * Returns the name of the JWT COOKIE
     */
    function jwt_cookie_name() {
        return env('JWT_COOKIE_NAME', 'JWT-IS');
    }
}

if (!function_exists('user')) {
    /**
     * Get logged in user
     */
    function user($key = '', $default = NULL, $jwt_key = NULL) {
        if ($jwt = jwt_decode($_COOKIE[jwt_cookie_name()] ?? NULL, $jwt_key)) {
            if ($user = @not_empty($jwt['user'])) {
                return !empty($key) ? $user[$key] ?? $default : $user;
            }
        }

        return $default;
    }
}

if (!function_exists('user_id')) {
    /**
     * Logged in user's id
     */
    function user_id() {
        return user('id', 0);
    }
}

if (!function_exists('check_login')) {
    /**
     * Checks if the user is logged-in (returns user_id) otherwise aborts
     */
    function check_login() {
        if ($user_id = user_id()) {
            return $user_id;
        }

        login_redir('Login required');
    }
}

if (!function_exists('site_set_cookie')) {
    /**
     * Sets a global cookie
     */
    function site_set_cookie($name, $value, $expires = 0) {
        if (setcookie($name, $value, !empty($expires) ? (is_numeric($expires) ? time() + $expires : strtotime($expires)) : 0, '/', "." . get_domain())) {
            $_COOKIE[$name] = $value;
        }
    }
}


if (!function_exists('user_login')) {
    /**
     * Log in a user by starting session
     */
    function user_login($user, $expires = 86400, $key = '') {
        if ($user = db_find('users', ['id' => is_numeric($user) ? $user : (is_array($user) ? $user['id'] : 0)])) {
            $user_data = user_info_public($user);
            site_set_cookie(jwt_cookie_name(), jwt_encode(array_merge(['user' => $user_data], ['exp' => time() + ($expires ?: 86400)]), $key), $expires);
            user_activity_insert('login', $user['id']);
            return $user;
        }

        return FALSE;
    }
}

if (!function_exists('user_logout')) {
    /**
     * Logs out existing user
     */
    function user_logout() {
        if ($user_id = user_id()) {
            user_activity_insert('logout', $user_id);
        }

        site_set_cookie(jwt_cookie_name(), '', -1000);
    }
}

if (!function_exists('user_activity_find')) {
    /**
     * Find a activity by name (or creates it)
     */
    function user_activity_find($activity_name) {
        if ($activity = db_find_cached(86400 * 30, 'activities', ['name' => $activity_name])) {
            return $activity['id'];
        } else {
            return db_insert('activities', ['name' => $activity_name, 'created_at' => mysql_date()]);
        }
    }
}

if (!function_exists('user_activity_insert')) {
    /**
     * Insert a user activity into user activity table
     */
    function user_activity_insert($activity_name, $user_id = NULL) {
        try {
            if ($uid = @(is_array($user_id) ? $user_id['id'] : (is_numeric($user_id) ? $user_id : user_id()))) {
                $activity_id = user_activity_find($activity_name);
                return db_insert('user_activities', ['user_id' => $uid, 'activity_id' => $activity_id, 'created_at' => mysql_date()], FALSE);
            }
        } catch (\Exception $e) {
        }

        return FALSE;
    }
}

if (!function_exists('user_info_public')) {
    /**
     * All publicly accessible user info
     */
    function user_info_public($user = NULL) {
        $user = $user ?: user();

        if (!empty($user)) {
            $data = pluck_keys_ignore_empty($user, 'id', 'ident', 'email', 'username', 'phone', 'created_at', 'verified');

            if (!empty($user['user_data'])) {
                $data = array_merge($data ?: [], is_array($user['user_data']) ? $user['user_data'] : (json_decode_array($user['user_data']) ?: []));
                unset($data['user_data']);
            }

            return $data;
        } else {
            return ['id' => 0];
        }
    }
}

if (!function_exists('guest_id')) {
    /**
     * Checks if the user is authenticated in guest mode and if so returns the user_id (false otherwise)
     */
    function guest_id($jwt_key = NULL) {
        $user = user('', NULL, $jwt_key);

        return (!empty($user['id']) && empty($user['email'])) ? $user['id'] : 0;
    }
}

if (!function_exists('check_captcha')) {
    /**
     * Checks if there is a need to verify captcha
     */
    function check_captcha() {
        if (!empty(env('RECAPTCHA_SECRET3'))) {
            $url = 'https://www.google.com/recaptcha/api/siteverify';

            if (!empty($_COOKIE['captcha_bypass']) && ($data = jwt_decode($_COOKIE['captcha_bypass']))) {
                if ($data['bypass'] == 'true') {
                    return TRUE;
                }
            }

            if (!empty(@$_POST['recaptcha2'])) {
                $data = json_decode_array(curl($url, 'POST', ['secret' => env('RECAPTCHA_SECRET2'), 'response' => @$_POST['recaptcha2']]));

                if (!empty($data) && ($data['success'] == 'true')) {
                    site_set_cookie('captcha_bypass', jwt_encode(['bypass' => TRUE, 'exp' => time() + 300]));
                } else {
                    abort('CAPTCHA verification failed.');
                }
            } else if ($data = json_decode_array(curl($url, 'POST', ['secret' => env('RECAPTCHA_SECRET3'), 'response' => @$_POST['recaptcha3']]))) {
                if (($data['success'] != 'true') || ($data['score'] <= 0.3)) {
                    abort('bot');
                }
            }
        }

        return TRUE;
    }
}

if (!function_exists('bootstrap')) {
    /**
     * Handles setting up environment for handling http requests
     */
    function bootstrap($env) {
        dot_env($env);

        if (!is_cli()) {
            @session_set_cookie_params(86400, '/', '.' . get_domain(), FALSE, TRUE);
            @session_start();

            handle_json_request();
            handle_cors_request();

            if (!in_array(get_http_method(), ['GET', 'POST'])) {
                exit(0);
            } elseif (env('CSRF_ENABLED') === 'true') {
                if (get_http_method() === 'GET') {
                    if (empty($_SESSION['XSRF-TOKEN'])) {
                        $_SESSION['XSRF-TOKEN'] = password(32);
                    }

                    setcookie('XSRF-TOKEN', $_SESSION['XSRF-TOKEN'], 0, '/', "." . get_domain());
                } elseif (get_http_method() === 'POST') {
                    if (!empty($_SESSION['XSRF-TOKEN']) && (empty($_SERVER['HTTP_X_XSRF_TOKEN']) || ($_SERVER['HTTP_X_XSRF_TOKEN'] !== $_SESSION['XSRF-TOKEN']))) {
                        abort('CSRF token mismatch (please reload website)');
                    }
                }
            }

            if (is_ajax_request()) {
                header('Content-Type: application/json', TRUE);
            }
        }

        if ($dsn = env('DB_NAME')) {
            if ($fn = env('DB_DEBUG')) {
                if ($log = app_dir($fn)) {
                    @unlink($log);
                }
            }

            db_connect();
        }

        putenv('APP_BOOTED=true');
    }
}
